defmodule AtxStation.Sensors do
  @moduledoc """
  A helper to find the w1 sensor name.
  """
  @sensor_path "/sys/bus/w1/devices/"

  def find_sensors do
    {:ok, list} = File.ls(@sensor_path)

    list
    |> Enum.reject(fn x -> x == "w1_bus_master1" end)
    |> Enum.each(fn x-> IO.puts "#{x}" end)
  end
end