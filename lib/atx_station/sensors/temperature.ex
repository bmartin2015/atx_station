defmodule AtxStation.Sensors.Temperature do
  @moduledoc """
  Functions for reading temperature data from a DS18B20 digital Temperature
  sensor.
  """

  @doc """
  Read the current temperature from the sensor in *C
  """
  @spec read() :: Float.t()
  defdelegate read(), to: AtxStation.Sensors.Temperature.Server, as: :read
end
