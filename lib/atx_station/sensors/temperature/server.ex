defmodule AtxStation.Sensors.Temperature.Server do
  @moduledoc false
  use GenServer

  alias AtxStation.Sensors.Temperature.Impl

  def start_link(_) do
    GenServer.start_link(__MODULE__, [], name: :temperature_sensor)
  end

  def read() do
    GenServer.call(:temperature_sensor, :read)
  end

  # GenServer API

  @impl GenServer
  def init(_) do
    {:ok, %{}}
  end

  @impl GenServer
  def handle_call(:read, _from, _state) do
    {:reply, Impl.read(), %{}}
  end
end