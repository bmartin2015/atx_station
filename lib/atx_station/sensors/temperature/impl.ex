defmodule AtxStation.Sensors.Temperature.Impl do
  @moduledoc false

  @sensor_path "/sys/bus/w1/devices/28-0317207d9fff/w1_slave"

  @spec read() :: float
  def read do
    @sensor_path
    |> File.read!()
    |> parse_temp()
  end

  defp parse_temp(data) do
    {temp, _} =
      Regex.run(~r/t=(\d+)/, data)
      |> List.last()
      |> Float.parse()

    temp / 1000
  end
end